#!/bin/bash

USER="$1"
NEWREPO="$2"

git clone "ssh://${USER}@osm.etsi.org:29418/TEMPLATE" && (cd "TEMPLATE" && curl https://osm.etsi.org/gerrit/tools/hooks/commit-msg > .git/hooks/commit-msg ; chmod +x .git/hooks/commit-msg)
git clone "ssh://${USER}@osm.etsi.org:29418/osm/${NEWREPO}" && (cd "${NEWREPO}" && curl https://osm.etsi.org/gerrit/tools/hooks/commit-msg > .git/hooks/commit-msg ; chmod +x .git/hooks/commit-msg)

cp -r TEMPLATE/* ${NEWREPO}/
cp TEMPLATE/.gitignore ${NEWREPO}/
rm ${NEWREPO}/copytemplate.sh

cd ${NEWREPO}
git add -A
git commit -s -m "Copy TEMPLATE files to this repo"
git push origin HEAD:refs/for/master

